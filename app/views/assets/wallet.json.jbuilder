json.error false
json.wallet do
  json.user_id @current_user.id
  json.tokens @wallet.tokens
  json.tokens_value @wallet.tokens_value
  json.tokens_value_raw @wallet.tokens_raw
  json.total_tokens_value @current_user.tokens_value
  json.last_refill @wallet.last_refill
  btc_raw = @wallet.btc_raw
  json.btc_raw btc_raw
  json.btc_value (btc_raw * 9000).round(0)
  eth_raw = @wallet.eth_raw
  json.eth_raw eth_raw
  json.eth_value (eth_raw * 450).round(0)
  est_raw = @wallet.est_raw
  json.est_raw est_raw
  json.est_value (est_raw * 0.6).round(0)
end
