json.error false
json.properties do
  json.array! @owned_properties do |property_owner|
      property = property_owner.property
      json.property_id property.id
      json.address (property.address || Faker::Address.street_address)
      json.description Faker::Address.community
      json.name (property.name || Faker::Address.community)
      json.tokens property_owner.tokens
      token_unit = 4.31
      json.token_unit token_unit
      token_value = (property_owner.tokens * token_unit).round(2)
      btc_unit = rand(8000..9000)
      btc_raw = token_value / btc_unit
      json.btc_unit btc_unit
      json.btc_raw (btc_raw).round(4)
      json.token_value "$#{token_value}"
      json.timestamp "#{time_ago_in_words(property_owner.created_at)} ago"
      json.created_at property_owner.created_at.strftime("%d/%m/%Y")
      json.type property_owner.tokens.to_f.negative? ? 'Sell' : "Buy"
  end
end
