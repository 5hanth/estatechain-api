json.error false
json.properties do
  json.array! @properties do |property|
    json.id property.id
    json.address (property.address || Faker::Address.street_address)
    json.name (property.name || Faker::Address.community)
    json.description Faker::Address.community
    token_value = property.token_value || 4.341
    json.token_price "$#{token_value}"
    json.total_investment "$#{number_with_delimiter(Faker::Number.number(6))}"
    json.investment_pending "$#{number_with_delimiter(Faker::Number.number(5))}"
    json.total_investors (541..1000).to_a.sample
    json.net_rental_yield "#{Faker::Number.between(5,10.0).round(2)}%"
    json.estimated_roi "#{Faker::Number.between(5,10.0).round(2)}%"
    json.tokens_left property.tokens_left
    json.days_left (1..100).to_a.sample
    json.net_rental_dividend "#{Faker::Number.between(5,10.0).round(2)}%"
    json.estimated_value_growth "#{Faker::Number.between(2,5.0).round(2)}%"
    json.total_estimated_growth "#{Faker::Number.between(2,5.0).round(2)}%"
    json.rank (1..98).to_a.sample
    json.rank_total 98
    json.updated_at "#{time_ago_in_words(Faker::Time.between(DateTime.now - 6.hours, DateTime.now))}"
  end
end
