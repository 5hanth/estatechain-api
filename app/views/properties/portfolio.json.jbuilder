json.error false
json.properties do
  json.array! @properties do |property|
    json.id property.id
    json.color (property.color || "##{SecureRandom.hex(3)}")
    json.img_url (property.img_url || "http://fineearth.in/wp-content/uploads/2016/02/3-BHKvilla-copy.jpg")
    json.address (property.address || Faker::Address.street_address)
    json.name (property.name || Faker::Address.community)
    json.description Faker::Address.community
    bought_price = rand(4.341 .. 7.1).round(2)
    token_value = (property.token_value || bought_price).round(2)
    json.token_price "$#{token_value}"
    json.bought_price bought_price
    current_price = rand(7.1001 .. 10.1).round(2)
    json.current_price current_price
    json.growth_percent (((current_price - bought_price) / bought_price ) * 100).round(2)
    json.tokens_owned @current_user.tokens_for(property.id)
    json.total_investment "$#{number_with_delimiter(token_value * @current_user.tokens_for(property.id))}"
    json.investment_pending "$#{number_with_delimiter(Faker::Number.number(5))}"
    json.total_investors (541..1000).to_a.sample
    json.net_rental_yield "#{Faker::Number.decimal(2,2)}%"
    json.estimated_roi "#{Faker::Number.decimal(2,2)}%"
    json.tokens_left property.tokens_left
    json.days_left (1..100).to_a.sample
    json.net_rental_dividend "#{Faker::Number.between(2,5.0).round(2)}%"
    json.estimated_value_growth "#{Faker::Number.between(2,5.0).round(2)}%"
    json.total_estimated_growth "#{Faker::Number.between(2,5.0).round(2)}%"
    json.rank (1..98).to_a.sample
    json.rank_total 98
  end
end

json.properties_invested @properties_invested
json.tokens_owned @tokens_owned
json.last_transaction "#{time_ago_in_words(@last_transaction)} ago" if @last_transaction.present?
json.value_of_tokens @value_of_tokens
json.wallet_tokens @wallet_tokens
