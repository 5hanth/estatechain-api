json.error false
json.property do
  json.id @property.id
  json.imgs1 @imgs.shuffle.take(3)
  json.imgs2 @imgs.shuffle.take(3)
  json.imgs3 @imgs.shuffle.take(3)
  json.video "http://fineearth.in/wp-content/uploads/2016/02/3-BHKvilla-copy.jpg"
  json.address (@property.address || Faker::Address.street_address)
  json.name (@property.name || Faker::Address.community)
  json.description Faker::Address.community
  token_value = @property.token_value || 4.341
  json.token_price "$#{token_value}"
  json.token_price_raw token_value
  json.total_investment "$#{number_with_delimiter(Faker::Number.number(6))}"
  json.investment_pending "$#{number_with_delimiter(Faker::Number.number(5))}"
  json.total_investors (541..1000).to_a.sample
  json.net_rental_yield "#{Faker::Number.between(5,10.0).round(2)}%"
  json.estimated_roi "#{Faker::Number.between(5,10.0).round(2)}%"
  json.about "This property has a strong investment case, with a calculated total return of 40% over 5 years based on third party forecasts for the region. By agreeing to buy four townhouses in a single transaction, we’ve secured a 7.6% discount to the price of the individual houses. The exit strategy for this investment will be to sell the houses individually to realise the full discount for investors. These stylish new-build townhouses in Goring-by-sea are finished to a high standard with private parking and gardens available for tenants. The properties are situated in a well-established residential area and benefit from close proximity to the seafront (0.5 miles away), as well as the South Downs (1.2 miles away), making this an attractive location to live. In addition, Goring-by-Sea railway station is less than a mile away with direct connections to Brighton (29 mins), Portsmouth & Southsea (50 mins) and London Victoria (1hr 25 mins). This should contribute to strong demand from commuting professionals and families"
  json.tokens_left @property.tokens_left
  json.days_left (1..100).to_a.sample
  json.net_rental_dividend "#{Faker::Number.between(2,5.0).round(2)}%"
  json.estimated_value_growth "#{Faker::Number.between(2,5.0).round(2)}%"
  json.total_estimated_growth "#{Faker::Number.between(2,5.0).round(2)}%"
  json.rank (1..98).to_a.sample
  json.rank_total 98
  json.location do
    location = [{:lat=>37.751, :lng=>-97.822}, {:lat=>37.751, :lng=>-97.822}, {:lat=>0.0, :lng=>0.0}, {:lat=>35.7977, :lng=>-78.6253}, {:lat=>0.0, :lng=>0.0}, {:lat=>37.751, :lng=>-97.822}, {:lat=>37.751, :lng=>-97.822}, {:lat=>32.8795, :lng=>-96.9398}, {:lat=>37.5112, :lng=>126.9741}, {:lat=>0.0, :lng=>0.0}, {:lat=>39.0481, :lng=>-77.4729}, {:lat=>34.7725, :lng=>113.7266}, {:lat=>32.404, :lng=>-86.2539}, {:lat=>37.3762, :lng=>-122.1826}, {:lat=>35.0757, :lng=>-106.6406}, {:lat=>42.3646, :lng=>-71.1028}, {:lat=>37.751, :lng=>-97.822}, {:lat=>0.0, :lng=>0.0}, {:lat=>0.0, :lng=>0.0}, {:lat=>35.69, :lng=>139.69}].sample
    json.lat location[:lat]
    json.lng location[:lng]
  end
end
