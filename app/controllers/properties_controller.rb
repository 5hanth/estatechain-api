class PropertiesController < ApplicationController

  before_action :authenticate_user, except: [:index, :show, :trends]

  def index
    @imgs = img_arr
    begin
      @properties = Property.all
    rescue => e
      render json: { error: true, msg: e.message }
    end
  end

  def trends
    begin
      @properties = Property.all
    rescue => e
      render json: { error: true, msg: e.message }
    end
  end

  def show
    begin
      @property = Property.find(params[:id])
      if @property.img_url
        url = @property.img_url
        @imgs = (1..17).map do |i|
          "http://s3.us-east-2.amazonaws.com/estate-assets/properties/c#{i}-01.jpg"
        end
      else
        @imgs = img_arr
      end
    rescue => e
      render json: { error: true, msg: e.message }
    end
  end

  def portfolio
    begin
      @current_user = current_user
      property_ids = current_user.owned_properties.pluck(:property_id).uniq
      @properties = Property.where(id: property_ids)
      @properties_invested = property_ids.count
      @tokens_owned = current_user.owned_properties.pluck(:tokens).map(&:to_f).sum.round(2)
      @wallet_tokens = current_user.wallet.tokens
      @value_of_tokens = "$#{(@tokens_owned * 4.341).round(2)}"
      @latest_property = current_user.owned_properties.last
      if @latest_property.present?
        @last_transaction = @latest_property.created_at
      end
    rescue => e
      render json: { error: true, msg: e.message }
    end
  end

  def chart
    begin
      owned_ids = current_user.owned_properties.pluck(:property_id).uniq
      tokens_owned = current_user.owned_properties.pluck(:tokens).map(&:to_f).sum
      chart_map = {labels: [], data: [], colors: []}
      owned_ids.each do |id|
        property = Property.find(id)
        chart_map[:labels] << property.name
        chart_map[:colors] << (property.color || "##{SecureRandom.hex(3)}")
        prop_tokens = current_user.owned_properties.where(property_id: id).pluck(:tokens).map(&:to_f).sum
        chart_map[:data] << ((prop_tokens / tokens_owned).round(2) * 100).to_i
      end
      render json: { error: false, chart: chart_map }
    rescue => e
      render json: { error: true, msg: e.message }
    end
  end

  private

  def property_params
    # params.require(:property).permit()
  end

  def img_arr
    imgs = []
    3.times do
      imgs << "http://fineearth.in/wp-content/uploads/2016/02/3-BHKvilla-copy.jpg"
    end
    imgs
  end

end
