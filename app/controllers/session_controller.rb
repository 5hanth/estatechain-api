class SessionController < ApplicationController

  before_action :authenticate_user, only: []

  def register
    begin
      user = User.create!(user_params)
      jwt = Knock::AuthToken.new(payload: { sub: user.id }).token
      render json: { error: false, user: user.details, jwt: jwt }
    rescue => e
      render json: { error: true, msg: e.message }
    end
  end

  private

  def user_params
    params.require(:user).permit(:email, :password)
  end

end
