class AssetsController < ApplicationController

  before_action :authenticate_user, except: [:index, :show]

  def create
    begin
      raise "Insuffient balance to buy property" if current_user.wallet.tokens < buy_params[:tokens].to_f
      property = Property.find(buy_params[:id])
      raise "Insuffient tokens available for the property" if property.tokens < buy_params[:tokens].to_f
      current_user.owned_properties.create!(property: property, tokens: buy_params[:tokens])
      render json: { error: false, property: property }
    rescue => e
      render json: { error: true, msg: e.message }
    end
  end

  def refill_tokens
    begin
      wallet = current_user.wallet
      raise "Tokens can be refilled only after 6 hours from previous refilling. Last refill : #{wallet.last_refill}" unless wallet.last_refill < 6.hour.ago
      wallet.refill_tokens
      render json: { error: false, tokens: wallet.tokens }
    rescue => e
      render json: { error: true, msg: e.message }
    end
  end

  def transactions
    begin
      @owned_properties = current_user.owned_properties.order(created_at: :desc)
    rescue => e
      render json: { error: true, msg: e.message }
    end
  end

  def wallet
    begin
      wallet = current_user.wallet
      @current_user = current_user
      @wallet  = wallet
      # render json: { error: false, wallet: { user_id: current_user.id, tokens: wallet.tokens, tokens_value: wallet.tokens_value, tokens_value_raw: wallet.tokens_raw, total_tokens_value: current_user.tokens_value, last_refill: wallet.last_refill} }
    rescue => e
      render json: { error: true, msg: e.message }
    end
  end

  private

  def buy_params
    params.require(:property).permit(:id, :tokens)
  end

end
