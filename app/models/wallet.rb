class Wallet < ApplicationRecord
  belongs_to :user

  def refill_tokens
    if last_refill < 6.hour.ago
      self.update(tokens: self.tokens + 500, last_refill: Time.now)
    end
  end

  def tokens_value
    "$#{(tokens.to_f * 4.31).round(2)}"
  end

  def tokens_raw
    (tokens.to_f * 4.31).round(2)
  end

  def btc_raw
    rand(0.1 .. 20.1).round(2)
  end

  def btc_value
  end

  def eth_raw
    rand(0.1 .. 20.1).round(2)
  end

  def eth_value
  end

  def est_raw
    rand(0.1 .. 20000.1).round(2)
  end

  def est_value
  end

end
