class User < ApplicationRecord

  validates :email, presence: {message: 'cannot be blank'}
  validates :email, uniqueness: {message: 'already registered'}

  has_secure_password

  has_many :owned_properties, class_name: 'PropertyOwner'
  has_one :wallet

  after_commit :create_wallet, on: :create

  def details
    { name: name, email: email }
  end

  def tokens_for(property_id)
    owned_properties.where(property_id: property_id).pluck(:tokens).map(&:to_f).sum.round(2)
  end

  def tokens_value
    "$#{((owned_properties.pluck(:tokens).map(&:to_f).sum * 4.31) + wallet.tokens_raw).round(2)}"
  end

  private
  def create_wallet
    self.create_wallet!(tokens: 1000)
  end

end
