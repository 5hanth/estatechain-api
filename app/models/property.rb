class Property < ApplicationRecord

  has_many :sales, class_name: 'PropertyOwner'

  after_commit :refill_tokens

  def tokens_left
    (self.tokens - tokens_sold)
  end

  def tokens_sold
    self.sales.pluck(:tokens).map(&:to_f).sum.to_f
  end

  private
  def refill_tokens
    return unless tokens_left < 0
    self.update(tokens: tokens_sold * 2)
  end
end
