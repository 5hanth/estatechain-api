class PropertyOwner < ApplicationRecord
  belongs_to :property
  belongs_to :user

  after_commit :reduce_wallet_tokens, on: :create

  private

  def reduce_wallet_tokens
    wallet = user.wallet
    wallet.update(tokens: wallet.tokens - self.tokens)
  end

end
