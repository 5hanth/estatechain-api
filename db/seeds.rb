# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#
# 12.times do
#   Property.create(address: Faker::Address.street_address, description: Faker::Address.community)
#   json.address Faker::Address.street_address
#   json.description Faker::Address.community
#   json.token_price "4.5$"
#   json.total_investment "$#{number_with_delimiter(Faker::Number.decimal(6,2))}"
#   json.investment_pending "$#{number_with_delimiter(Faker::Number.decimal(5,2))}"
#   json.total_investors (541..1000).to_a.sample
#   json.net_rental_yield "#{Faker::Number.decimal(2,2)}%"
#   json.estimated_roi "#{Faker::Number.decimal(2,2)}%"
#   json.about "This property has a strong investment case, with a calculated total re.."
#   json.tokens_left property.tokens_left
#   json.days_left (1..100).to_a.sample
#   json.net_rental_dividend "#{Faker::Number.decimal(2,2)}%"
#   json.estimated_value_growth "#{Faker::Number.decimal(1,2)}%"
#   json.total_estimated_growth "#{Faker::Number.decimal(1,2)}%"
#   json.rank (1..98).to_a.sample
#   json.rank_total 98
#   json.location do
#     json.lat "-#{Faker::Number.decimal(2,2)}"
#     json.lng "#{Faker::Number.decimal(3,3)}"
#   end
# end
