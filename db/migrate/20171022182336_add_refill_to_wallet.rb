class AddRefillToWallet < ActiveRecord::Migration[5.1]
  def change
    add_column :wallets, :last_refill, :datetime, :default => -> { 'CURRENT_TIMESTAMP' }
  end
end
