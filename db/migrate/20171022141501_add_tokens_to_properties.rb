class AddTokensToProperties < ActiveRecord::Migration[5.1]
  def change
    add_column :properties, :tokens, :decimal, precision: 8, scale: 2
    add_column :property_owners, :tokens, :decimal, precision: 8, scale: 2
  end
end
