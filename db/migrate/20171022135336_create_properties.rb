class CreateProperties < ActiveRecord::Migration[5.1]
  def change
    create_table :properties do |t|
      t.string :name
      t.decimal :token_value, :precision => 8, :scale => 2 
      t.string :address
      t.string :img_url

      t.timestamps
    end
  end
end
