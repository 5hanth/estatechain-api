class CreatePropertyOwners < ActiveRecord::Migration[5.1]
  def change
    create_table :property_owners do |t|
      t.belongs_to :property, foreign_key: true
      t.belongs_to :user, foreign_key: true

      t.timestamps
    end
  end
end
