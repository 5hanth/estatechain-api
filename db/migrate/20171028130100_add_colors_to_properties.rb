class AddColorsToProperties < ActiveRecord::Migration[5.1]
  def change
    add_column :properties, :color, :string, default: '#425c7a'
  end
end
