Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  post 'user_token' => 'user_token#create'
  post 'register' => 'session#register'

  get 'properties' => 'properties#index'
  get 'properties/:id' => 'properties#show'
  get 'trends' => 'properties#trends'

  post 'buy' => 'assets#create'
  get 'chart' => 'properties#chart'
  post 'refill' => 'assets#refill_tokens'
  get 'transactions' => 'assets#transactions'
  get 'wallet' => 'assets#wallet'
  get 'portfolio' => 'properties#portfolio'

end
